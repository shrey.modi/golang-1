package main

import "fmt"

func factorial(num int) int {
	if num == 1 || num == 0 {
		return num
	}
	return num * factorial(num-1)
}
func main() {

	fmt.Println("Enter Your number : ")

	var num int
	// Taking input from user
	fmt.Scanln(&num)
	fmt.Printf("FACTORIAL IS : ")
	fmt.Println(factorial(num))

}
