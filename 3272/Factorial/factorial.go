package main

import (
	"fmt"
)

func factorial(n int) int {
	if n < 0 {
		panic("Input must be a non-negative integer.")
	}
	result := 1
	for i := 2; i <= n; i++ {
		result *= i
	}
	return result
}

func main() {
	var n int
	fmt.Print("Enter a non-negative integer: ")
	_, err := fmt.Scanln(&n)
	if err != nil {
		panic("Invalid input.")
	}
	fmt.Printf("The factorial of %d is %d\n", n, factorial(n))
}
