package main

import "fmt"

func binarySearch(element int, arr []int) bool {

    start := 0
    end := len(arr) - 1

    for start <= end {
        m := (start + end) / 2

        if arr[m] < element {
            start = m + 1
        } else {
            end = m - 1
        }
    }

    if start == len(arr) || arr[start] != element {
        return false
    }

    return true
}

func main() {
    arr := []int{2, 5, 6, 14, 22, 35, 49, 58, 69, 71, 84, 99}
    fmt.Println(binarySearch(22, arr))
    fmt.Println(binarySearch(12, arr))
    fmt.Println(binarySearch(5, arr))
}
