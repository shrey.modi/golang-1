package main

import (
	"fmt"
	"sync"
	"time"
)

func sum(s []int) int {
	total := 0
	// for i := 0; i < len(s); i++ {
	// 	total += s[i]
	// }
	for _, h := range s {
		total += h
	}
	return total
}

func hello() {
	time.Sleep(1 * time.Second)
	fmt.Println("Harsh")
	wg.Done()
}

var wg sync.WaitGroup

func main() {

	s := []int{1, 2, 3}

	wg.Add(3)
	go hello()
	go hello()
	go hello()

	fmt.Println(sum(s))
	wg.Wait()
	// time.Sleep(1 * time.Second)
}
