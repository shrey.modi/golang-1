package main

import "fmt"

func binSearch(a []int, search int) (result int, Count int) {
    mid := len(a) / 2
    switch {
    case len(a) == 0:
        result = -1 
    case a[mid] > search:
        result, searchCount = binarySearch(a[:mid], search)
    case a[mid] < search:
        result, searchCount = binarySearch(a[mid+1:], search)
        if result >= 0 { 
            result += mid + 1
        }
    default: 
        result = mid 
    }
    Count++
    return
}