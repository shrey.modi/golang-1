// Binarysearch program using golang
package main

import "fmt"

func main() {
	arr := []int{5, 10, 15, 20, 25, 30}
	begin, end, mid, target, ind := 0, len(arr)-1, 0, 30, -1
	for begin <= end {
		mid = begin + (end-begin)/2
		if arr[mid] == target {
			ind = mid
			break
		} else if arr[mid] < target {
			begin = mid + 1
		} else {
			end = mid - 1
		}
	}
	if ind == -1 {
		fmt.Println("element not found")
	} else {
		fmt.Println("element found at", ind)
	}
}
