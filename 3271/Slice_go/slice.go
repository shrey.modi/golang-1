// Slice implementation in GO Lang
package main

import "fmt"

func main() {
	array1 := [5]int{1, 3, 0}
	slice1 := []int{1, 4, 8}

	fmt.Println(array1)
	fmt.Println(slice1)

	slice1 = append(slice1, 6)
	fmt.Println(slice1)
	fmt.Println("slice1[1:2]", slice1[1:2])
	slice1 = slice1[1:]
	fmt.Println(slice1)

}
