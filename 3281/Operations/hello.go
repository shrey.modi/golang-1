//It performs operations on array as sum, slice, print and compare on arrays

package main

import "fmt"

func main(){
	array1 := [5]int{4, 5, 7}
	var array2 = [5]int{4, 5, 7}
	slice_demo := []int{1,2}
	ans:=sum(array1)
	fmt.Println(ans)


	if array1 == array2 {
		fmt.Println("array1 and array2 are equal")
	}else {
		fmt.Println("not equal")
	}

	for i := 0; i < len(array1); i++{
		fmt.Printf("%d ", array1[i]);
	}
	fmt.Println()
	slice_demo=slice1[1:]
	fmt.Println(slice_demo)
}
