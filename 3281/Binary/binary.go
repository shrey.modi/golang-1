//This program perform a binary search on an array. It prints the index on which the element is found and in how many counts.
package main

import "fmt"

func binSearch(a []int, search int) (result int, Count int) {
    mid := len(a) / 2
    switch {
    case len(a) == 0:
        result = -1 
    case a[mid] > search:
        result, search = binSearch(a[:mid], search)
    case a[mid] < search:
        result, search = binSearch(a[mid+1:], search)
        if result >= 0 { 
            result += mid + 1
        }
    default: 
        result = mid 
    }
    Count++
    
    return
}


func main(){
	items := []int{1,2, 9, 20, 31, 45, 63, 70, 100}
    result, Count := binSearch(items, 63)
    fmt.Println(result, Count)
}
