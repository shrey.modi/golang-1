// learning pointers variables functions conditional statements and for loop
package main

import "fmt"

var temp = 1008

type apple int8

func main() {
	//var msg = "abc"  //normal variable declaration
	var abc string
	msg := "Hellu" //short variable declaration   This cannot be done at package level, can only be done in function

	fmt.Println("Hello World!")
	fmt.Println(msg, string(rune(temp)), "Nothing ", abc)

	var bar apple
	fmt.Println(bar)
	p := func1()
	fmt.Println(*p)
	// foo1()

	//to call a function we have to write "go run thisfilename.go functionfilename.go"

	array1 := [5]int{1, 2, 3}
	array2 := [5]int{1, 2, 3, 4}

	if array1 == array2 {
		fmt.Print("Equal")
	} else {
		fmt.Println("Not equal")
	}
	fmt.Println(array1)
	fmt.Println(array2)

	for i := 0; i < len(array2); i++ {
		fmt.Printf("%d ", array2[i])
	}
	fmt.Println()

}

func func1() *int {
	var a int = 5
	return &a
}

//DEFAULT VALUES:
//int = 0
//string = empty string
