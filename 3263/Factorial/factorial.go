// Program to find the factorial of the number
package main

import "fmt"

func main() {
	var num, factorial int
	fmt.Print("Enter the number: ") // to prompt the user
	fmt.Scan(&num)                  // take the input from the user
	factorial = 1
	for i := 1; i <= num; i++ {
		factorial *= i
	}
	fmt.Printf("Factorial of %d is %d", num, factorial)
}
