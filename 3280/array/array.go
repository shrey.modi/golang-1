// implementing if-else statement and for loop using array

package main

import (
	"fmt"
)

func array() {
	array1 := [5]int{4, 5, 7}
	var array2 = [5]int{4, 5, 6, 7}

	if array1 == array2 {
		fmt.Println("array1 is equal to array2")
	} else if array1 != array2 {
		fmt.Println(array2)
	} else {
		fmt.Println(array1)
	}

	for i := 0; i < len(array2); i++ {
		fmt.Printf("%d ", array2[i])
	}
	fmt.Println()

	fmt.Println(array1)
}

func main() {
	array()
}
