package main

import "fmt"

func factorial(num int) int {

    ans := 1
    for i := 1; i <= num; i++ {
        ans = ans * i
    }
    return ans

}

func main() {
    fmt.Print("Enter number: ")

    // var then variable name then variable type
    var num int

    // Taking input from user
    fmt.Scanln(&num)

    ans := factorial(num)

    fmt.Printf("%d! = %d", num, ans)

}
