package main

import "fmt"

func main() {
	var arr = [8]int{1,2,3,4,5,6,9,7}

	var a = 9
	var flag = 0

	for i := 0 ; i < len(arr) ; i++ {
		if(a == arr[i]){
			flag = 1
			break
		} else {
			flag = 0
		}
	}

	if(flag == 1){
		fmt.Print("Element found!!!")
	} else if(flag == 0) {
		fmt.Print("Element not found!!!")
	}
}