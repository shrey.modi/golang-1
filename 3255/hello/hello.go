// This program is for basic introduction to go
package main

import "fmt"

var tmp = 35

func main() {
	msg := "Hello World!"
	fmt.Println("Hello GO,", msg, tmp)
	fmt.Println("calling foo")
	foo()

	array1 := [5]int{4, 5, 7}
	array2 := []int{4, 5, 7}
	// if array1 == array2 {
	// 	fmt.Println("array1 and array2 is same")
	// } else {
	// 	fmt.Println("arrays are not same")
	// }

	for i := 0; i < len(array1); i++ {
		fmt.Printf("%d ", array1[i])
	}
	array2 = array2[1:]
	fmt.Println(array2)

	map1 := map[string]int{
		"foo": 2,
		"bar": 45,
	}
	map1["searce"] = 96
	fmt.Println(map1)
	// fmt.Println(sum(array2))
}
