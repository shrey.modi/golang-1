// program to demonstrate array and slices operations
package main

import "fmt"

// to return sum in slice
func sum(slice1 []int) int {
	var sum_arr int
	for i := 0; i < len(slice1); i++ {
		sum_arr += slice1[i]
	}

	return sum_arr
}

func main() {
	// declaring arrays
	arr1 := [5]int{4, 5, 6, 7}
	var arr2 = [5]int{4, 5, 6, 7}

	// checking if arrays are equal or not
	if arr1 == arr2 {
		fmt.Println("arrays are same")
	} else {
		fmt.Println("arrays are not same")
	}

	fmt.Println(arr1)

	// looping thru array
	for i := 0; i < len(arr1); i++ {
		fmt.Printf("%d ", arr1[i])
	}

	// slice
	slice1 := []int{1, 3}
	fmt.Println(slice1)

	fmt.Println(arr1)

	slice1 = append(slice1, 6)
	fmt.Println(slice1)

	_ = append(slice1, 4)

	// slicing
	fmt.Println("slice1[1:2] =", slice1[0:2])
	fmt.Println("slice1[1:] =", slice1[1:])
	// fmt.Println("slice1[:2] =", slice1[:2])
	sum_arr := sum(slice1)

	fmt.Println(sum_arr)

}
