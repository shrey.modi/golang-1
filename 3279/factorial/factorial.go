// program to calculate factorial of a given number
package main

import "fmt"

// function to calculate factorial of given number
func fact(num int) int {
	if num == 0 || num == 1 {
		return 1
	}
	return num * fact(num-1)
}

func main() {
	var num int
	fmt.Print("Enter a num: ")
	fmt.Scan(&num)
	ans := fact(num)
	fmt.Println("Factorial of", num, "is", ans)
}
