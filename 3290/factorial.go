package main
import "fmt"
func factorial(num int) int{
   if num == 1 || num == 0{
      return num
   }
   return num * factorial(num-1)
}
func main(){
   fmt.Println("Enter the number: ")
   var num int
   fmt.Scanln(&num)
   var fact int = factorial(num)
   fmt.Println("The Factorial of ",num, "is", fact)
}