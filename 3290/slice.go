package main

import "fmt"

func main() {
	array1 := [5]int{4,5,7}
	slice1 :=[]int{1,2,5,8}
	
	fmt.Println(array1)
	fmt.Println(slice1)

	slice1 = append(slice1,6)
	_ = append(slice1,6)

	fmt.Println(slice1)
	fmt.Println("slice[1:4]", slice1[1:4])
	slice1 = slice1[1:]
	fmt.Println(slice1)
}